package helpers

import (
	"go-service-fiber/domain"
	"net/http"
)

func SuccessResponse(data interface{}, responseCode string) *domain.Response {
	return &domain.Response{
		Status:     true,
		StatusCode: http.StatusText(http.StatusOK),
		RC:         responseCode,
		Result:     data,
	}
}

func BadRequestResponse(data interface{}, responseCode string) *domain.Response {
	return &domain.Response{
		Status:     false,
		StatusCode: http.StatusText(http.StatusBadRequest),
		RC:         responseCode,
		Result:     data,
	}
}

func UnauthorizedResponse(data interface{}, responseCode string) *domain.Response {
	return &domain.Response{
		Status:     false,
		StatusCode: http.StatusText(http.StatusUnauthorized),
		RC:         responseCode,
		Result:     data,
	}
}

func RequestTimeOutResponse(data interface{}, responseCode string) *domain.Response {
	return &domain.Response{
		Status:     false,
		StatusCode: http.StatusText(http.StatusRequestTimeout),
		RC:         responseCode,
		Result:     data,
	}
}
