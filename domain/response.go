package domain

type Response struct {
	Status     bool        `json:"status"`
	StatusCode string      `json:"statusCode"`
	RC         string      `json:"responseCode"`
	Result     interface{} `json:"result"`
}

// Pagination represents pagination for ListResponse
type Pagination struct {
	TotalPage int64 `json:"total_page"`
	TotalData int64 `json:"total_data"`
}

// ListResponse represents how list is returned as `data` in BaseResponse
type ListResponse struct {
	List       interface{} `json:"list"`
	Pagination Pagination  `json:"pagination"`
}
