package domain

import (
	"time"

	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

type Article struct {
	ID        uint   `json:"id" gorm:"primaryKey"`
	Title     string `json:"title" validate:"required"`
	Content   string `json:"content" validate:"required"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}

// ArticleUsecase represent the article's usecases
type ArticleUsecase interface {
	Fetch(ctx *fiber.Ctx) ([]Article, string, error)
}

// ArticleRepository represent the article's repository contract
type ArticleRepository interface {
	Fetch(ctx *fiber.Ctx) (res []Article, err error)
	GormFetch(ctx *fiber.Ctx) (result *Article, err error)
}
