package domain

import "github.com/gofiber/fiber/v2"

// RabbitMqMessageBrokerRepository represent the article's repository contract
type RabbitMqMessageBrokerRepository interface {
	PublishMessage(ctx *fiber.Ctx, msg string) (err error)
}
