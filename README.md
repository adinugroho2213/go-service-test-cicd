# Golang Service Backend

Repository for Backend services, writen in Go.

## Clean Architecture

This repo implements Uncle Bob's Clean Architecture.

Rule of Clean Architecture:

- Independent of Frameworks. The architecture does not depend on the existence of some library of feature laden software.
  This allows you to use such frameworks as tools, rather than having to cram your system into their limited constraints.
- Testable. The business rules can be tested without the UI, Database, Web Server, or any other external element.
- Independent of UI. The UI can change easily, without changing the rest of the system. A Web UI could be replaced
  with a console UI, for example, without changing the business rules.
- Independent of Database. You can swap out Oracle or SQL Server, for Mongo, BigTable, CouchDB, or something else.
  Your business rules are not bound to the database.
- Independent of any external agency. In fact your business rules simply don’t know anything at all about
  the outside world.

### References:

- https://github.com/bxcodec/go-clean-arch
- https://medium.com/@imantumorang/golang-clean-archithecture-efd6d7c43047
- https://github.com/koddr/tutorial-go-fiber-rest-api

## How to run locally

This repo requires Go 1.17.

### Method 1: using go run

1. Clone this repository to your local run `git clone http://10.100.111.95/KalKausar/go-fiber-service.git {project-name}`
   - Example : `git clone http://10.100.111.95/KalKausar/go-fiber-service.git service-dwh`
2. Change remote url git : `git remote set-url origin {url_destination_git}`
   - Example : `git remote set-url origin https://github.com/USERNAME/REPOSITORY.git`
3. Edit your own config in `.env`.
4. Create folder log in directory
5. Run `go run app/main.go`.

Go modules will automatically download all dependencies before the service starts. If there's no error, the service is
available at `localhost:9990` (unless you've changed the port number).

## Further Questions

Should there be any further questions, please ask the maintainer.
