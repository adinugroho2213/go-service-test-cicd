package main

import (
	"fmt"
	"log"
	"os"
	"reflect"

	"go-service-fiber/helpers"
	_articleHttpDelivery "go-service-fiber/pkg/article/delivery"
	_articleRepo "go-service-fiber/pkg/article/repository"
	_articleUcase "go-service-fiber/pkg/article/usecase"
	config "go-service-fiber/pkg/common/config"
	"go-service-fiber/pkg/common/consumer"
	_messagesbrokerRepo "go-service-fiber/pkg/messagebroker/repository"
	utilCache "go-service-fiber/util/cache"
	"go-service-fiber/util/cron"
	util "go-service-fiber/util/db"
	"go-service-fiber/util/mb"

	"github.com/gofiber/contrib/fiberzerolog"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog"
)

func main() {
	app := fiber.New()

	apiGroup := app.Group("/go-service-fiber")
	// Read config

	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Some error occured. Err: %s", err)
	}

	env := os.Getenv("ENVIRONMENT_APPLICATION")

	if env == "" {
		env = "local"
	}

	cfg, err := config.Read(fmt.Sprintf("environment/%s.env", env))
	if err != nil {
		log.Fatalln("read config err:", err)
	}

	// Init utils: http server, db connection, etc.
	utils := initUtils(cfg)

	app.Use(recover.New())

	// Or extend your config for customization
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowHeaders: "Origin, Content-Type, Accept, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization",
		AllowMethods: "OPTIONS, GET, POST, PUT, DELETE",
	}))
	file, err := os.OpenFile(fmt.Sprintf("log/apps.log"), os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer file.Close()

	loggerApps := zerolog.New(file).With().Timestamp().Logger()
	app.Use(fiberzerolog.New(fiberzerolog.Config{
		Logger: &loggerApps,
		Fields: []string{helpers.FieldLatency, helpers.FieldStatus, helpers.FieldMethod, helpers.FieldURL, helpers.FieldError, helpers.FieldQueryParams, helpers.FieldBody, helpers.FieldReqHeaders, helpers.FieldResBody, helpers.FieldReqHeaders},
	}))

	fileSystem, err := os.OpenFile(fmt.Sprintf("log/system.log"), os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer fileSystem.Close()

	log.SetOutput(zerolog.New(fileSystem).With().Timestamp().Logger())

	if cfg.RabbitMq.RabbitMqConsumer {
		// Subscribing to QueueService1 for getting messages.
		messages, err := utils.RabbitMqConnection.RabbitMQChannel.Consume(
			cfg.RabbitMq.RabbitMqQueue, // queue name
			"",                         // consumer
			true,                       // auto-ack
			false,                      // exclusive
			false,                      // no local
			false,                      // no wait
			nil,                        // arguments
		)
		if err != nil {
			log.Println(fmt.Sprintf("Error Consume Message to RabbitMQ |Error %s", err.Error()))
		}

		// Build a welcome message.
		log.Println("Successfully connected to RabbitMQ")
		log.Println("Waiting for messages")

		go func() {
			for message := range messages {
				log.Println(fmt.Sprintf("Received message from producer: %s", string(message.Body)))
				consumer.ConsumerMessageBroker(string(message.Body))
			}
		}()
	}

	// Init repository and use case layer
	_, uc, err := initRepoAndUseCases(utils, cfg)
	if err != nil {
		log.Fatalln("initRepoAndUseCases err:", err)
	}

	_articleHttpDelivery.NewArticleHandler(apiGroup, uc.ArticleUseCase)

	//inisiasiCron
	cron := cron.NewCron()
	registerCron(cron, uc)
	cron.Start()

	log.Fatal(app.Listen(cfg.Http.Port))
}

func initUtils(cfg config.Config) AppUtil {
	dbConn, err := util.New(cfg.Database)
	if err != nil {
		log.Println("Error while connecting to database at", cfg.Database.Host, err.Error())
	}

	gormConn, err := util.NewGorm(cfg.Database)
	if err != nil {
		log.Println("Error while connecting to gorm database at", cfg.Database.Host, err.Error())
	}

	redisConn, err := utilCache.New(cfg.Redis)
	if err != nil {
		log.Println("Error while connecting to redis at", cfg.Redis.RedisPort, err.Error())
	}

	var amqpChannel *mb.RabbitMQChannel
	if cfg.RabbitMq.RabbitMqProducer {
		amqpChannel, err = mb.New(cfg.RabbitMq)
		if err != nil {
			log.Println("Error while connecting to rabbit mq at", cfg.Redis.RedisPort, err.Error())
		}
	}

	return AppUtil{
		DbConnection:       dbConn,
		Cron:               cron.NewCron(),
		RedisConnection:    redisConn,
		RabbitMqConnection: amqpChannel,
		GormConnection:     gormConn,
	}
}

// initRepoAndUseCases initialises repo and use case layer
func initRepoAndUseCases(util AppUtil, cfg config.Config) (repo AppRepo, uc AppUseCase, err error) {
	repo.ArticleRepo = _articleRepo.NewPostgresArticleRepository(util.DbConnection.DbConnection, util.GormConnection.DbConnection)
	if cfg.RabbitMq.RabbitMqProducer {
		repo.MessageBrokerRepo = _messagesbrokerRepo.NewRabbitMqMessageBrokerRepository(util.RabbitMqConnection.RabbitMQChannel, cfg.RabbitMq)
	}

	uc.ArticleUseCase = _articleUcase.NewArticleUsecase(repo.ArticleRepo)

	return repo, uc, nil
}

func registerCron(c *cron.Cron, uc AppUseCase) {
	h := reflect.ValueOf(uc)

	for i := 0; i < h.NumField(); i++ {
		handler, ok := h.Field(i).Interface().(cron.CronHandler)
		if ok {
			// if it implements CronHandler, register it!
			handler.RegisterCron(c)
		}
	}
}

type AppUtil struct {
	DbConnection       *util.DatabaseConnection
	GormConnection     *util.GormConnection
	Cron               *cron.Cron
	RedisConnection    *utilCache.RedisClient
	RabbitMqConnection *mb.RabbitMQChannel
}

type AppUseCase struct {
	ArticleUseCase *_articleUcase.ArticleUsecase
}

type AppRepo struct {
	ArticleRepo       *_articleRepo.PostgresArticleRepository
	MessageBrokerRepo *_messagesbrokerRepo.RabbitMqMessageBrokerRepository
}
