package repository

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"

	"go-service-fiber/domain"
)

type PostgresArticleRepository struct {
	Conn *sqlx.DB
	Gorm *gorm.DB
}

// NewPostgresArticleRepository will create an object that represent the article.Repository interface
func NewPostgresArticleRepository(Conn *sqlx.DB, Gorm *gorm.DB) *PostgresArticleRepository {
	return &PostgresArticleRepository{
		Conn: Conn,
		Gorm: Gorm,
	}
}

func (m *PostgresArticleRepository) fetch(ctx *fiber.Ctx, query string) (result []domain.Article, err error) {
	rows, err := m.Conn.Query(query)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	defer func() {
		errRow := rows.Close()
		if errRow != nil {
			logrus.Error(errRow)
		}
	}()

	result = make([]domain.Article, 0)
	for rows.Next() {
		t := domain.Article{}
		err = rows.Scan(
			&t.ID,
			&t.Title,
			&t.Content,
		)

		if err != nil {
			logrus.Error(err)
			return nil, err
		}
		result = append(result, t)
	}

	return result, nil
}

func (m *PostgresArticleRepository) Fetch(ctx *fiber.Ctx) (res []domain.Article, err error) {
	query := `SELECT id,title,content FROM article `

	res, err = m.fetch(ctx, query)
	if err != nil {
		return nil, err
	}
	return
}

func (m *PostgresArticleRepository) GormFetch(ctx *fiber.Ctx) (result *domain.Article, err error) {
	var article domain.Article

	test := m.Gorm.First(&article)
	if test.Error != nil {
		fmt.Println(test.Error)
	}
	// fmt.Println(article)
	return &article, nil
}
