package usecase

import (
	"fmt"
	"go-service-fiber/domain"
	"go-service-fiber/helpers"

	"github.com/gofiber/fiber/v2"
)

type ArticleUsecase struct {
	articleRepo domain.ArticleRepository
}

// NewArticleUsecase will create new an articleUsecase object representation of domain.ArticleUsecase interface
func NewArticleUsecase(a domain.ArticleRepository) *ArticleUsecase {
	return &ArticleUsecase{
		articleRepo: a,
	}
}

/*
* In this function below, I'm using errgroup with the pipeline pattern
* Look how this works in this package explanation
* in godoc: https://godoc.org/golang.org/x/sync/errgroup#ex-Group--Pipeline
 */

func (a *ArticleUsecase) Fetch(c *fiber.Ctx) (res []domain.Article, responseCode string, err error) {
	var test *domain.Article
	// res, err = a.articleRepo.Fetch(c)
	// if err != nil {
	// 	return nil, helpers.RC["GENERAL_ERROR"], err
	// }
	// if len(res) == 0 {
	// 	return nil, helpers.RC["NODATA"], err
	// }

	test, _ = a.articleRepo.GormFetch(c)

	fmt.Println(test)

	return res, helpers.RC["SUCCESS"], nil
}
