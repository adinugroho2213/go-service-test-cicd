package delivery

import (
	"net/http"

	"go-service-fiber/domain"
	"go-service-fiber/helpers"

	"github.com/gofiber/fiber/v2"
)

// ArticleHandler  represent the httphandler for article
type ArticleHandler struct {
	AUsecase domain.ArticleUsecase
}

// NewArticleHandler will initialize the articles/ resources endpoint
func NewArticleHandler(r fiber.Router, us domain.ArticleUsecase) {
	handler := &ArticleHandler{
		AUsecase: us,
	}
	r.Get("/articles", handler.FetchArticle)
}

// FetchArticle will fetch the article based on given params
func (a ArticleHandler) FetchArticle(c *fiber.Ctx) error {
	listAr, responseCode, err := a.AUsecase.Fetch(c)
	if err != nil {
		return c.Status(http.StatusBadRequest).JSON(helpers.BadRequestResponse(fiber.Map{"message": err}, responseCode))
	}
	return c.Status(http.StatusOK).JSON(helpers.SuccessResponse(listAr, responseCode))
}
