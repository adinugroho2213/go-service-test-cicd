package messagebroker

import (
	"fmt"
	"go-service-fiber/pkg/common/config"
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/streadway/amqp"
)

type RabbitMqMessageBrokerRepository struct {
	RabbitMQChannel *amqp.Channel
	ConfigRabbitMq  config.RabbitMqConfig
}

func NewRabbitMqMessageBrokerRepository(RabbitMqChannel *amqp.Channel, ConfigRabbitMq config.RabbitMqConfig) *RabbitMqMessageBrokerRepository {
	return &RabbitMqMessageBrokerRepository{RabbitMqChannel, ConfigRabbitMq}
}

func (r *RabbitMqMessageBrokerRepository) PublishMessage(c *fiber.Ctx, msg string) (err error) {
	// Create a message to publish.
	message := amqp.Publishing{
		ContentType: "text/plain",
		Body:        []byte(msg),
	}

	// Attempt to publish a message to the queue.
	if err := r.RabbitMQChannel.Publish(
		"",                             // exchange
		r.ConfigRabbitMq.RabbitMqQueue, // queue name
		false,                          // mandatory
		false,                          // immediate
		message,                        // message to publish
	); err != nil {
		log.Println(fmt.Sprintf("Error Publish Message to RabbitMQ |Error %s|Message %s", err.Error(), msg))
		return err
	}
	defer r.RabbitMQChannel.Close()

	return nil
}
