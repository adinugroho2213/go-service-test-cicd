package consumer

import "github.com/rs/zerolog/log"

func ConsumerMessageBroker(msg string) (err error) {
	log.Info().Msg(msg)
	return
}
