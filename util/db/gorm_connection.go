package util

import (
	"fmt"
	"go-service-fiber/pkg/common/config"
	"log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type GormConnection struct {
	DbConnection *gorm.DB
}

// New constructs new DatabaseConnection
func NewGorm(config config.DatabaseConfig) (*GormConnection, error) {
	connStr := ConnectGorm(config)

	return &GormConnection{
		DbConnection: connStr,
	}, nil
}

func GetGORMConfig(config config.DatabaseConfig) string {
	pgsql := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%d sslmode=disable sslmode=disable TimeZone=Asia/Jakarta",
		config.Host, config.User, config.Password, config.DbName, config.Port,
	)
	return pgsql
}

func ConnectGorm(config config.DatabaseConfig) *gorm.DB {
	var (
		db  *gorm.DB
		err error
	)
	if db, err = gorm.Open(postgres.Open(GetGORMConfig(config)), &gorm.Config{}); err != nil {
		log.Fatal(err)
	}
	return db
}
