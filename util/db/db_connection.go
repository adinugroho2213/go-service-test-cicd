package util

import (
	"fmt"
	"log"

	"go-service-fiber/pkg/common/config"

	_ "github.com/lib/pq"

	"github.com/jmoiron/sqlx"
)

type DatabaseConnection struct {
	DbConnection *sqlx.DB
}

// New constructs new DatabaseConnection
func New(config config.DatabaseConfig) (*DatabaseConnection, error) {
	connStr := Connect(config)

	return &DatabaseConnection{
		DbConnection: connStr,
	}, nil
}

func GetPGSQLConfig(config config.DatabaseConfig) string {
	pgsql := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		config.Host, config.Port, config.User, config.Password, config.DbName,
	)
	return pgsql
}

func Connect(config config.DatabaseConfig) *sqlx.DB {
	var (
		db  *sqlx.DB
		err error
	)
	if db, err = sqlx.Open("postgres", GetPGSQLConfig(config)); err != nil {
		log.Fatal(err)
	}
	return db
}
