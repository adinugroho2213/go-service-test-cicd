package mb

import (
	"fmt"
	"go-service-fiber/pkg/common/config"
	"log"

	"github.com/streadway/amqp"
)

type RabbitMQChannel struct {
	RabbitMQChannel *amqp.Channel
}

// New constructs new MessageBroker Connection
func New(config config.RabbitMqConfig) (*RabbitMQChannel, error) {
	connStr := Connect(config)

	return &RabbitMQChannel{
		RabbitMQChannel: connStr,
	}, nil
}

func Connect(config config.RabbitMqConfig) *amqp.Channel {
	var (
		rmqChannel *amqp.Channel
		err        error
	)

	// Define RabbitMQ server URL.
	amqpServerURL := fmt.Sprintf("amqp://%s:%s@%s:%s/", config.RabbitMqUsername, config.RabbitMqPassword, config.RabbitMqHost, config.RabbitMqPort)

	// Create a new RabbitMQ connection.
	connectRabbitMQ, err := amqp.Dial(amqpServerURL)
	if err != nil {
		log.Println(fmt.Sprintf("Cannot Connect to RabbitMQ Connection |Error %s", err.Error()))
	}
	// defer connectRabbitMQ.Close()

	// Let's start by opening a channel to our RabbitMQ
	// instance over the connection we have already
	// established.
	rmqChannel, err = connectRabbitMQ.Channel()
	if err != nil {
		log.Println(fmt.Sprintf("Cannot Open Channel to RabbitMQ Connection |Error %s", err.Error()))
	}
	// defer channelRabbitMQ.Close()

	// With the instance and declare Queues that we can
	// publish and subscribe to.
	_, err = rmqChannel.QueueDeclare(
		config.RabbitMqQueue, // queue name
		true,                 // durable
		false,                // auto delete
		false,                // exclusive
		false,                // no wait
		nil,                  // arguments
	)
	if err != nil {
		log.Println(fmt.Sprintf("Cannot Declare Channel to RabbitMQ Connection |Error %s", err.Error()))
	}

	return rmqChannel
}
